package com.adilparvez.forum.execptions

import org.springframework.boot.web.servlet.error.ErrorController
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import java.time.Clock
import javax.servlet.http.HttpServletRequest

@Controller
@RequestMapping("/error")
class ErrorController(
        private val clock: Clock
) : ErrorController {

    @RequestMapping
    fun index(request: HttpServletRequest): ResponseEntity<ErrorResponse> {
        val exception = request.getAttribute("javax.servlet.error.exception") as Exception
        val response = ErrorResponse(clock.instant(), HttpStatus.INTERNAL_SERVER_ERROR, exception.cause?.message ?: "")
        return ResponseEntity(response, response.status)
    }

    override fun getErrorPath(): String? = null
}