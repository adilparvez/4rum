package com.adilparvez.forum.execptions

import org.springframework.http.HttpStatus
import java.time.Instant

data class ErrorResponse(
        val time: Instant,
        val status: HttpStatus,
        val details: Any
)