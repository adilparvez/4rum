package com.adilparvez.forum.execptions

import org.springframework.http.HttpStatus
import java.lang.RuntimeException

class ApiException(
        val httpStatus: HttpStatus,
        override val message: String = "",
        override val cause: Throwable? = null
) : RuntimeException(message, cause)