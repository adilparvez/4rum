package com.adilparvez.forum.execptions

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.validation.FieldError
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import java.time.Clock
import java.util.*


@ControllerAdvice
class ExceptionHandlers(
        private val clock: Clock
) {

    @ExceptionHandler(MethodArgumentNotValidException::class)
    fun handleMethodArgumentNotValid(ex: MethodArgumentNotValidException): ResponseEntity<ErrorResponse> {
        val errors = HashMap<String, String?>()
        ex.bindingResult.allErrors.forEach { error ->
            val fieldName = (error as FieldError).field
            val errorMessage = error.getDefaultMessage()
            errors[fieldName] = errorMessage
        }

        val response = ErrorResponse(clock.instant(), HttpStatus.BAD_REQUEST, errors)
        return ResponseEntity(response, response.status)
    }

    @ExceptionHandler(HttpMessageNotReadableException::class)
    fun handleHttpMessageNotReadable(ex: HttpMessageNotReadableException): ResponseEntity<ErrorResponse> {
        val response = ErrorResponse(clock.instant(), HttpStatus.BAD_REQUEST, "malformed json")
        return ResponseEntity(response, response.status)
    }

    @ExceptionHandler(ApiException::class)
    fun handleApiException(ex: ApiException): ResponseEntity<ErrorResponse> {
        val response = ErrorResponse(clock.instant(), ex.httpStatus, ex.message)
        return ResponseEntity(response, response.status)
    }

}