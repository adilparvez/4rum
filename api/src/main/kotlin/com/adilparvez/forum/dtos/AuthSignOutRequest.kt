package com.adilparvez.forum.dtos

import javax.validation.constraints.NotBlank

data class AuthSignOutRequest(
        @field:NotBlank
        val refreshToken: String?
)