package com.adilparvez.forum.dtos

import java.time.Instant

data class PostDto(
        val id: Long,
        val title: String,
        val text: String,
        val topic: String,
        val createdBy: String,
        val createdAt: Instant
)