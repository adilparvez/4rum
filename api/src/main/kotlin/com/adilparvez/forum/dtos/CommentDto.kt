package com.adilparvez.forum.dtos

import java.time.Instant

data class CommentDto(
        val id: Long,
        val parentId: Long?,
        val text: String,
        val postId: Long,
        val createdBy: String,
        val createdAt: Instant
)