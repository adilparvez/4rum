package com.adilparvez.forum.dtos

import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

data class CommentCreateRequest(
        @field:NotNull
        val postId: Long?,
        val parentId: Long?,
        @field:NotBlank
        val text: String?
)