package com.adilparvez.forum.dtos

import javax.validation.constraints.NotBlank

data class AuthSignUpRequest(
        @field:NotBlank
        val username: String?,
        @field:NotBlank
        val password: String?
)