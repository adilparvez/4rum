package com.adilparvez.forum.dtos

import javax.validation.constraints.NotBlank

data class TopicCreateRequest(
        @field:NotBlank
        val name: String?,
        @field:NotBlank
        val description: String?
)
