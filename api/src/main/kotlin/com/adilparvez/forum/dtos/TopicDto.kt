package com.adilparvez.forum.dtos

import java.time.Instant

data class TopicDto(
        val id: Long,
        val name: String,
        val description: String,
        val createdBy: String,
        val createdAt: Instant,
        val numPosts: Long
)