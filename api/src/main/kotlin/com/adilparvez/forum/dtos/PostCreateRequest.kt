package com.adilparvez.forum.dtos

import javax.validation.constraints.NotBlank

data class PostCreateRequest(
        @field:NotBlank
        val topic: String?,
        @field:NotBlank
        val title: String?,
        @field:NotBlank
        val text: String?
)