package com.adilparvez.forum.dtos

import javax.validation.constraints.NotBlank

data class AuthGetAuthTokenRequest(
        @field:NotBlank
        val refreshToken: String?
)