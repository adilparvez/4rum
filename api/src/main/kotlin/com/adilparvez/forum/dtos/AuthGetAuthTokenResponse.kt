package com.adilparvez.forum.dtos

data class AuthGetAuthTokenResponse(
        val authToken: String
)