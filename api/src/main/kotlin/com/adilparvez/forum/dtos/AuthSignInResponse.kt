package com.adilparvez.forum.dtos

data class AuthSignInResponse(
        val authToken: String,
        val refreshToken: String
)