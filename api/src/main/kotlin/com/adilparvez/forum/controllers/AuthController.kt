package com.adilparvez.forum.controllers

import com.adilparvez.forum.dtos.*
import com.adilparvez.forum.services.AuthService
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/auth")
class AuthController(
        private val authService: AuthService
) {

    @PostMapping("/sign-in")
    fun signIn(@Valid @RequestBody request: AuthSignInRequest): AuthSignInResponse {
        return authService.signIn(request)
    }

    @PostMapping("/sign-up")
    @ResponseStatus(HttpStatus.CREATED)
    fun signUp(@Valid @RequestBody request: AuthSignUpRequest) {
        authService.signUp(request)
    }

    @PostMapping("/sign-out")
    fun signOut(@Valid @RequestBody request: AuthSignOutRequest) {
        authService.signOut(request)
    }

    @PostMapping("/sign-out-all")
    fun signOutAll(@Valid @RequestBody request: AuthSignOutRequest) {
        authService.signOutAll(request)
    }

    @PostMapping("/get-auth-token")
    fun getAuthToken(@Valid @RequestBody request: AuthGetAuthTokenRequest): AuthGetAuthTokenResponse {
        return authService.getAuthToken(request)
    }

}