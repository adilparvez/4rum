package com.adilparvez.forum.controllers

import com.adilparvez.forum.dtos.PostCreateRequest
import com.adilparvez.forum.dtos.PostDto
import com.adilparvez.forum.models.User
import com.adilparvez.forum.security.CurrentUser
import com.adilparvez.forum.services.PostService
import org.springframework.web.bind.annotation.*
import springfox.documentation.annotations.ApiIgnore
import javax.validation.Valid

@RestController
@RequestMapping("/api/post")
class PostController(
        private val postService: PostService
) {

    @PostMapping
    fun create(
            @Valid @RequestBody request: PostCreateRequest,
            @ApiIgnore @CurrentUser user: User
    ) {
        postService.create(request, user)
    }

    @GetMapping("/by-user/{username}")
    fun getByUser(@PathVariable username: String): List<PostDto> {
        return postService.getByUser(username)
    }

    @GetMapping("/by-topic/{name}")
    fun getByTopic(@PathVariable name: String): List<PostDto> {
        return postService.getByTopic(name)
    }

}