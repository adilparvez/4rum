package com.adilparvez.forum.controllers

import com.adilparvez.forum.dtos.TopicCreateRequest
import com.adilparvez.forum.dtos.TopicDto
import com.adilparvez.forum.models.User
import com.adilparvez.forum.security.CurrentUser
import com.adilparvez.forum.services.TopicService
import org.springframework.web.bind.annotation.*
import springfox.documentation.annotations.ApiIgnore
import javax.validation.Valid

@RestController
@RequestMapping("/api/topic")
class TopicController(
        private val topicService: TopicService
) {

    @PostMapping
    fun create(
            @Valid @RequestBody request: TopicCreateRequest,
            @ApiIgnore @CurrentUser user: User
    ) {
        topicService.create(request, user)
    }

    @GetMapping("/{name}")
    fun getTopic(@PathVariable name: String): TopicDto {
        return topicService.getTopic(name)
    }

    @GetMapping
    fun getTopics(): List<TopicDto> {
        return topicService.getTopics()
    }

}