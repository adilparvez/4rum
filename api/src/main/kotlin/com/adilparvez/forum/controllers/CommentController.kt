package com.adilparvez.forum.controllers

import com.adilparvez.forum.dtos.CommentCreateRequest
import com.adilparvez.forum.dtos.CommentDto
import com.adilparvez.forum.dtos.CommentTreeDto
import com.adilparvez.forum.models.User
import com.adilparvez.forum.security.CurrentUser
import com.adilparvez.forum.services.CommentService
import org.springframework.web.bind.annotation.*
import springfox.documentation.annotations.ApiIgnore
import javax.validation.Valid

@RestController
@RequestMapping("/api/comment")
class CommentController(
        private val commentService: CommentService
) {

    @PostMapping
    fun create(
            @Valid @RequestBody request: CommentCreateRequest,
            @ApiIgnore @CurrentUser user: User
    ) {
        commentService.create(request, user)
    }

    @GetMapping("/by-user/{username}")
    fun getByUser(@PathVariable username: String): List<CommentDto> {
        return commentService.getByUser(username)
    }

    @GetMapping("/by-post/{id}")
    fun getByPost(@PathVariable id: Long): List<CommentTreeDto> {
        return commentService.getByPost(id)
    }

}