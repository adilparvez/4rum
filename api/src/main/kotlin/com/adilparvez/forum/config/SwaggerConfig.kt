package com.adilparvez.forum.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.bind.annotation.RestController
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.service.ApiKey
import springfox.documentation.service.AuthorizationScope
import springfox.documentation.service.SecurityReference
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spi.service.contexts.SecurityContext
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2


@Configuration
@EnableSwagger2
class SwaggerConfig {

    @Bean
    fun api(): Docket {
        return Docket(DocumentationType.SWAGGER_2)
                .select()
                    .apis(RequestHandlerSelectors.withClassAnnotation(RestController::class.java))
                    .paths(PathSelectors.any())
                    .build()
                .securitySchemes(listOf(apiKey()))
                .securityContexts(listOf(securityContext()))
    }

    @Bean
    fun securityContext(): SecurityContext {
        return SecurityContext.builder()
                .securityReferences(defaultAuth())
                .forPaths(PathSelectors.any())
                .build()
    }

    private fun apiKey(): ApiKey = ApiKey("Bearer", "Authorization", "header")

    private fun defaultAuth(): List<SecurityReference> {
        val authScopes = arrayOf(AuthorizationScope("global", "accessEverything"))
        return listOf(SecurityReference("Bearer", authScopes))
    }

}