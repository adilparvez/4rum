package com.adilparvez.forum.models

import java.time.Instant
import javax.persistence.*

@Entity
@Table(name = "topic")
data class Topic(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(columnDefinition = "SERIAL")
        val id: Long? = null,
        val name: String,
        val description: String,
        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "userId", referencedColumnName = "id")
        val user: User,
        val created: Instant,

        @OneToMany(fetch = FetchType.LAZY, mappedBy = "topic")
        val posts: List<Post> = emptyList()
)