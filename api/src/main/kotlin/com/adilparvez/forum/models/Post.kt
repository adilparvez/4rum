package com.adilparvez.forum.models

import java.time.Instant
import javax.persistence.*

@Entity
@Table(name = "post")
data class Post(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(columnDefinition = "SERIAL")
        val id: Long? = null,
        val title: String,
        val text: String,
        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "userId", referencedColumnName = "id")
        val user: User,
        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "topicId", referencedColumnName = "id")
        val topic: Topic,
        val created: Instant,

        @OneToMany(fetch = FetchType.LAZY, mappedBy = "post")
        val comments: List<Comment> = emptyList()
)