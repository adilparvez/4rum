package com.adilparvez.forum.models

import javax.persistence.*

@Entity
@Table(name = "refresh_token")
data class RefreshToken(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(columnDefinition = "SERIAL")
        val id: Long? = null,
        val token: String,
        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "userId", referencedColumnName = "id")
        val user: User
)