package com.adilparvez.forum.models

import javax.persistence.*

@Entity
@Table(name = "_user")
data class User(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(columnDefinition = "SERIAL")
        val id: Long? = null,
        @Column(unique = true)
        val username: String,
        val password: String,

        @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
        val posts: List<Post> = emptyList(),

        @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
        val comments: List<Comment> = emptyList()
)