package com.adilparvez.forum.models

import java.time.Instant
import javax.persistence.*

@Entity
@Table(name = "comment")
data class Comment(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(columnDefinition = "SERIAL")
        val id: Long? = null,
        @OneToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "parentId", referencedColumnName = "id")
        val parent: Comment? = null,
        val text: String,
        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "postId", referencedColumnName = "id")
        val post: Post,
        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "userId", referencedColumnName = "id")
        val user: User,
        val created: Instant
)
