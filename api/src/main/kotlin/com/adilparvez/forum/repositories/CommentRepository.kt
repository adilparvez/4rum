package com.adilparvez.forum.repositories

import com.adilparvez.forum.models.Comment
import com.adilparvez.forum.models.Post
import com.adilparvez.forum.models.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface CommentRepository : JpaRepository<Comment, Long> {

    fun findByPost(post: Post): List<Comment>

    fun findByUser(user: User): List<Comment>

}