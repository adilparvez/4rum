package com.adilparvez.forum.repositories

import com.adilparvez.forum.models.RefreshToken
import com.adilparvez.forum.models.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface RefreshTokenRepository : JpaRepository<RefreshToken, Long> {

    fun findByToken(token: String): RefreshToken?

    fun deleteByToken(token: String)

    fun deleteByUserId(userId: Long)

}