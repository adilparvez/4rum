package com.adilparvez.forum.repositories

import com.adilparvez.forum.models.Topic
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface TopicRepository : JpaRepository<Topic, Long> {

    fun findByName(name: String): Topic?

}