package com.adilparvez.forum.repositories

import com.adilparvez.forum.models.Post
import com.adilparvez.forum.models.Topic
import com.adilparvez.forum.models.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface PostRepository : JpaRepository<Post, Long> {

    fun findByTopic(topic: Topic): List<Post>

    fun findByUser(user: User): List<Post>

}