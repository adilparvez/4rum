package com.adilparvez.forum.services

import com.adilparvez.forum.dtos.PostCreateRequest
import com.adilparvez.forum.dtos.PostDto
import com.adilparvez.forum.execptions.ApiException
import com.adilparvez.forum.models.Post
import com.adilparvez.forum.models.User
import com.adilparvez.forum.repositories.PostRepository
import com.adilparvez.forum.repositories.TopicRepository
import com.adilparvez.forum.repositories.UserRepository
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import java.time.Clock

@Component
class PostService(
        private val postRepository: PostRepository,
        private val topicRepository: TopicRepository,
        private val userRepository: UserRepository,
        private val clock: Clock
) {

    fun create(request: PostCreateRequest, user: User) {
        val topic = topicRepository.findByName(request.topic!!) ?: throw ApiException(HttpStatus.BAD_REQUEST, "Topic not found")

        val post = Post(
                title = request.title!!,
                text = request.text!!,
                user = user,
                topic = topic,
                created = clock.instant()
        )

        postRepository.save(post)
    }

    fun getByUser(username: String): List<PostDto> {
        val user = userRepository.findByUsername(username) ?: throw ApiException(HttpStatus.BAD_REQUEST, "User not found")

        return user.posts.map(::mapToDto)
    }

    fun getByTopic(name: String): List<PostDto> {
        val topic = topicRepository.findByName(name) ?: throw ApiException(HttpStatus.BAD_REQUEST, "Topic not found")

        return topic.posts.map(::mapToDto)
    }

    private fun mapToDto(post: Post): PostDto {
        return PostDto(
                id = post.id!!,
                title = post.title,
                text = post.text,
                topic = post.topic.name,
                createdBy = post.user.username,
                createdAt = post.created
        )
    }

}