package com.adilparvez.forum.services

import com.adilparvez.forum.dtos.CommentCreateRequest
import com.adilparvez.forum.dtos.CommentDto
import com.adilparvez.forum.dtos.CommentTreeDto
import com.adilparvez.forum.execptions.ApiException
import com.adilparvez.forum.models.Comment
import com.adilparvez.forum.models.User
import com.adilparvez.forum.repositories.CommentRepository
import com.adilparvez.forum.repositories.PostRepository
import com.adilparvez.forum.repositories.UserRepository
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import java.time.Clock

@Component
class CommentService(
        private val commentRepository: CommentRepository,
        private val postRepository: PostRepository,
        private val userRepository: UserRepository,
        private val clock: Clock
) {

    fun create(request: CommentCreateRequest, user: User) {
        val post = postRepository.findByIdOrNull(request.postId) ?: throw ApiException(HttpStatus.BAD_REQUEST, "Post not found")
        val parent = request.parentId?.let { commentRepository.findByIdOrNull(it) }

        val comment = Comment(
                parent = parent,
                text = request.text!!,
                post = post,
                user = user,
                created = clock.instant()
        )

        commentRepository.save(comment)
    }

    fun getByUser(username: String): List<CommentDto> {
        val user = userRepository.findByUsername(username) ?: throw ApiException(HttpStatus.BAD_REQUEST, "User not found")

        return user.comments.map(::mapToDto)
    }

    fun getByPost(id: Long): List<CommentTreeDto> {
        val post = postRepository.findByIdOrNull(id) ?: throw ApiException(HttpStatus.BAD_REQUEST, "Post not found")

        return mapToDto(post.comments)
    }

    private fun mapToDto(comment: Comment): CommentDto {
        return CommentDto(
                id = comment.id!!,
                parentId = comment.parent?.id,
                text = comment.text,
                postId = comment.post.id!!,
                createdBy = comment.user.username,
                createdAt = comment.created
        )
    }

    private fun mapToDto(comments: List<Comment>): List<CommentTreeDto> {
        val roots = treeify(
                comments,
                { c -> c.id!! },
                { c -> c.parent?.id }
        )

        return roots.map(::mapToDto)
    }

    private fun mapToDto(node: Node<Comment>): CommentTreeDto {
        val comment = node.data

        return CommentTreeDto(
                id = comment.id!!,
                parentId = comment.parent?.id,
                text = comment.text,
                postId = comment.post.id!!,
                createdBy = comment.user.username,
                createdAt = comment.created,
                children = node.children.map(::mapToDto)
        )
    }

    data class Node<T>(
            var data: T,
            val children: MutableList<Node<T>> = mutableListOf()
    )

    private fun <T> treeify(items: List<T>, getId: (T) -> Long, getParentId: (T) -> Long?): List<Node<T>> {
        val lookup = mutableMapOf<Long, Node<T>>()
        val roots = mutableListOf<Node<T>>()

        for (item in items) {
            lookup[getId(item)] = Node(data = item)
        }

        for (item in items) {
            val node = lookup[getId(item)]!!
            val parentNode = getParentId(item)?.let { lookup[it]!! }

            if (parentNode == null) {
                roots.add(node)
            } else {
                parentNode.children.add(node)
            }
        }

        return roots;
    }

}