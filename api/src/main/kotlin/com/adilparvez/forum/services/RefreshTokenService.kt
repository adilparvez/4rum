package com.adilparvez.forum.services

import com.adilparvez.forum.execptions.ApiException
import com.adilparvez.forum.models.RefreshToken
import com.adilparvez.forum.models.User
import com.adilparvez.forum.repositories.RefreshTokenRepository
import com.adilparvez.forum.repositories.UserRepository
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Component
class RefreshTokenService(
        private val refreshTokenRepository: RefreshTokenRepository,
        private val userRepository: UserRepository
) {

    fun generateRefreshToken(userId: Long): String {
        val token = uuid()
        val user = userRepository.findByIdOrNull(userId)!!
        refreshTokenRepository.save(RefreshToken(token = token, user = user))

        return token;
    }

    private fun uuid(): String = UUID.randomUUID().toString()

    fun validateRefreshToken(token: String) {
        if (refreshTokenRepository.findByToken(token) == null) {
            throw ApiException(HttpStatus.UNAUTHORIZED, "Invalid refresh token")
        }
    }

    @Transactional
    fun revokeRefreshToken(token: String) {
        refreshTokenRepository.deleteByToken(token)
    }

    @Transactional
    fun revokeAllRefreshTokens(token: String) {
        val refreshToken = refreshTokenRepository.findByToken(token)!!
        refreshTokenRepository.deleteByUserId(refreshToken.user.id!!)
    }

    fun getUser(token: String): User? {
        return refreshTokenRepository.findByToken(token)?.user
    }

}