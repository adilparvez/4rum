package com.adilparvez.forum.services

import com.adilparvez.forum.dtos.*
import com.adilparvez.forum.execptions.ApiException
import com.adilparvez.forum.models.User
import com.adilparvez.forum.repositories.UserRepository
import com.adilparvez.forum.security.JwtHelper
import org.springframework.http.HttpStatus
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Component

@Component
class AuthService(
        private val authenticationManager: AuthenticationManager,
        private val jwtHelper: JwtHelper,
        private val userRepository: UserRepository,
        private val passwordEncoder: PasswordEncoder,
        private val refreshTokenService: RefreshTokenService
) {

    fun signIn(request: AuthSignInRequest): AuthSignInResponse {
        val auth = authenticationManager.authenticate(UsernamePasswordAuthenticationToken(request.username, request.password))
        SecurityContextHolder.getContext().authentication = auth

        val authToken = jwtHelper.generateAuthToken(request.username!!)
        val user = userRepository.findByUsername(request.username)!!
        val refreshToken = refreshTokenService.generateRefreshToken(user.id!!)

        return AuthSignInResponse(authToken, refreshToken)
    }

    fun signUp(request: AuthSignUpRequest) {
        if (userRepository.findByUsername(request.username!!) != null) {
            throw ApiException(HttpStatus.BAD_REQUEST, "User already exists")
        }

        val user = User(
                username = request.username,
                password = passwordEncoder.encode(request.password)
        )

        userRepository.save(user)
    }

    fun signOut(request: AuthSignOutRequest) {
        val token = request.refreshToken!!
        refreshTokenService.validateRefreshToken(token)

        refreshTokenService.revokeRefreshToken(token)
    }

    fun signOutAll(request: AuthSignOutRequest) {
        val token = request.refreshToken!!
        refreshTokenService.validateRefreshToken(token)

        refreshTokenService.revokeAllRefreshTokens(token)
    }

    fun getAuthToken(request: AuthGetAuthTokenRequest): AuthGetAuthTokenResponse {
        val token = request.refreshToken!!
        refreshTokenService.validateRefreshToken(token)

        val user = refreshTokenService.getUser(token)!!
        val authToken = jwtHelper.generateAuthToken(user.username)

        return AuthGetAuthTokenResponse(authToken)
    }

}