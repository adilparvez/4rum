package com.adilparvez.forum.services

import com.adilparvez.forum.dtos.TopicCreateRequest
import com.adilparvez.forum.dtos.TopicDto
import com.adilparvez.forum.execptions.ApiException
import com.adilparvez.forum.models.Topic
import com.adilparvez.forum.models.User
import com.adilparvez.forum.repositories.TopicRepository
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import java.time.Clock

@Component
class TopicService(
        private val topicRepository: TopicRepository,
        private val clock: Clock
) {

    fun create(request: TopicCreateRequest, user: User) {
        if (topicRepository.findByName(request.name!!) != null) {
            throw ApiException(HttpStatus.BAD_REQUEST, "Topic already exists")
        }

        val topic = Topic(
                name = request.name!!,
                description = request.description!!,
                user = user,
                created = clock.instant()
        )

        topicRepository.save(topic)
    }

    fun getTopic(name: String): TopicDto {
        val topic = topicRepository.findByName(name) ?: throw ApiException(HttpStatus.NOT_FOUND, "Topic not found")

        return mapToDto(topic)
    }

    fun getTopics(): List<TopicDto> {
        return topicRepository.findAll()
                .map(::mapToDto)
    }

    private fun mapToDto(topic: Topic): TopicDto {
        return TopicDto(
                id = topic.id!!,
                name = topic.name,
                description = topic.description,
                createdBy = topic.user.username,
                createdAt = topic.created,
                numPosts = topic.posts.size.toLong()
        )
    }

}