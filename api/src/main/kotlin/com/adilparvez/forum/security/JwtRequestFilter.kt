package com.adilparvez.forum.security

import com.adilparvez.forum.services.UserDetailsServiceImpl
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class JwtRequestFilter(
        private val userDetailsService: UserDetailsServiceImpl,
        private val jwtHelper: JwtHelper
) : OncePerRequestFilter() {

    override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse, filterChain: FilterChain) {
        val token = jwtHelper.getAuthToken(request)
        if (token != null && jwtHelper.validateAuthToken(token)) {
            val auth = jwtHelper.getAuthentication(token)
            SecurityContextHolder.getContext().authentication = auth
        }

        filterChain.doFilter(request, response)
    }

}