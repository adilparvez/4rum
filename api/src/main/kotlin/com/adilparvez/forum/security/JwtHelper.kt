package com.adilparvez.forum.security

import com.adilparvez.forum.services.UserDetailsServiceImpl
import io.jsonwebtoken.Claims
import io.jsonwebtoken.JwtParser
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import io.jsonwebtoken.security.Keys
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Component
import java.time.Clock
import java.util.*
import javax.servlet.http.HttpServletRequest

@Component
class JwtHelper(
        private val jwtProperties: JwtProperties,
        private val clock: Clock,
        private val userDetailsService: UserDetailsServiceImpl
) {

    private fun encodeAsUtf8(text: String): ByteArray = text.toByteArray(Charsets.UTF_8)

    fun generateAuthToken(username: String): String {
        val now = clock.instant()
        val expires = now.plusSeconds(jwtProperties.validSeconds)

        return Jwts.builder()
                .setHeaderParam("typ", "JWT")
                .signWith(
                        Keys.hmacShaKeyFor(encodeAsUtf8(jwtProperties.secret)),
                        SignatureAlgorithm.HS512
                )
                .setIssuer("4rum")
                .setSubject(username)
                .setIssuedAt(Date.from(now))
                .setExpiration(Date.from(expires))
                .compact()
    }

    fun getAuthToken(request: HttpServletRequest): String? {
        val authorization = request.getHeader("Authorization")
        if (authorization != null && authorization.startsWith("Bearer ")) {
            return authorization.substring(7)
        }

        return null
    }

    class ClockAdapter(private val clock: Clock) : io.jsonwebtoken.Clock {

        override fun now(): Date {
            return Date.from(clock.instant())
        }

    }

    private fun makeParser(): JwtParser = Jwts.parserBuilder()
            .setSigningKey(Keys.hmacShaKeyFor(encodeAsUtf8(jwtProperties.secret)))
            .setClock(ClockAdapter(clock))
            .build()

    fun validateAuthToken(token: String): Boolean {
        try {
            makeParser().parseClaimsJws(token)

            return true
        } catch (ex: Exception) {
            return false
        }
    }

    private fun getClaims(token: String): Claims = makeParser().parseClaimsJws(token).body

    private fun getUsername(token: String): String? = getClaims(token).subject

    fun getAuthentication(token: String): Authentication {
        val userDetails = userDetailsService.loadUserByUsername(getUsername(token))

        return UsernamePasswordAuthenticationToken(userDetails, null, userDetails.authorities)
    }

}