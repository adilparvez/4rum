package com.adilparvez.forum.security

import com.adilparvez.forum.models.User
import com.adilparvez.forum.repositories.UserRepository
import org.springframework.core.MethodParameter
import org.springframework.security.core.Authentication
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Component
import org.springframework.web.bind.support.WebArgumentResolver
import org.springframework.web.bind.support.WebDataBinderFactory
import org.springframework.web.context.request.NativeWebRequest
import org.springframework.web.method.support.HandlerMethodArgumentResolver
import org.springframework.web.method.support.ModelAndViewContainer

@Component
class CurrentUserResolver(
        private val userRepository: UserRepository
) : HandlerMethodArgumentResolver {

    override fun supportsParameter(parameter: MethodParameter): Boolean {
        return parameter.getParameterAnnotation(CurrentUser::class.java) != null
                && parameter.parameterType == User::class.java
    }

    override fun resolveArgument(parameter: MethodParameter, mavContainer: ModelAndViewContainer?, webRequest: NativeWebRequest, binderFactory: WebDataBinderFactory?): Any? {
        if (!supportsParameter(parameter)) {
            return WebArgumentResolver.UNRESOLVED
        }

        val principal = webRequest.userPrincipal!!
        val userDetails = (principal as Authentication).principal as UserDetails

        return userRepository.findByUsername(userDetails.username)!!
    }

}