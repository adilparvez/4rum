# 4rum
A small project implementing the API for a forum app (made to play with Sping Boot/Kotlin). The backend is built using [Spring Boot](https://spring.io/projects/spring-boot) in [Kotlin](https://kotlinlang.org/), with [PostgreSQL](https://www.postgresql.org/) as the database.

Users can sign up and create topics about a particular theme (e.g. language learning, programming, etc.), make posts on a topic, and comment on posts.

## Endpoints
- auth
    - POST sign-up - create a new user
    - POST sign-in - authenticate the user and get back auth and refresh tokens
        ```json
        {
            "authToken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpc3MiOiI0cnVtIiwic3ViIjoiYWRpbCIsImlhdCI6MTYwMjcxNzU3MywiZXhwIjoxNjAyNzIxMTczfQ.pbRnS26lPUluOSwEnKauusVjvYoS9cKPs_ljoL910ZKqyNqHyclkeg36f6hY3lvrxKeaUcJHXnEBYu8FtbRtVw",
            "refreshToken": "6fe81f2f-d3a4-402c-ab17-7b8e833ffb49"
        }
        ```
    - POST get-auth-token - use the refresh token to get a fresh auth token
    - POST sign-out - invalidate a refresh token
    - POST sign-out-all - invalidate all refresh tokens for the user, i.e. sign out of all devices
- api
    - topic
        - GET - get all topics created
            ```json
            [
                {
                    "id": 1,
                    "name": "programming",
                    "description": "A topic about programming!",
                    "createdBy": "adil",
                    "createdAt": "2020-10-14T23:20:21.962Z",
                    "numPosts": 0
                }
            ]
            ```
        - POST - create a new topic
        - GET :name - get a particular topic
    - post
        - POST - make a post
        - GET by-topic/:name - get all of the posts on a topic
            ```json
            [
                {
                    "id": 1,
                    "title": "First post",
                    "text": "This is a test post.",
                    "topic": "programming",
                    "createdBy": "adil",
                    "createdAt": "2020-10-14T23:21:29.208Z"
                }
            ]
            ```
        - GET by-user/:username - get all of the posts by a particular user
    - comment
        - POST - post a comment
        - GET by-post/:id - get all of the comments on a post as comment trees
            ```json
            [
                {
                    "id": 1,
                    "parentId": null,
                    "text": "Hello foo :)",
                    "postId": 1,
                    "createdBy": "adil",
                    "createdAt": "2020-10-14T23:22:59.801Z",
                    "children": [
                        {
                            "id": 3,
                            "parentId": 1,
                            "text": "OP is the best",
                            "postId": 1,
                            "createdBy": "adil",
                            "createdAt": "2020-10-14T23:23:49.445Z",
                            "children": []
                        }
                    ]
                },
                {
                    "id": 2,
                    "parentId": null,
                    "text": "Hello bar :>",
                    "postId": 1,
                    "createdBy": "adil",
                    "createdAt": "2020-10-14T23:23:18.902Z",
                    "children": []
                }
            ]
            ```
        - GET by-user/:username - get all of the comments by a particular user

There are nice error messages too, e.g.
```json
{
    "time": "2020-10-14T23:31:32.801Z",
    "status": "BAD_REQUEST",
    "details": {
        "password": "must not be blank"
    }
}

{
  "time": "2020-10-14T23:33:18.205Z",
  "status": "BAD_REQUEST",
  "details": "Topic not found"
}

{
  "time": "2020-10-14T23:35:38.870Z",
  "status": "BAD_REQUEST",
  "details": "malformed json"
}

{
  "time": "2020-10-14T23:35:56.943Z",
  "status": "UNAUTHORIZED"
}
```

## Running
To bring the project up locally run the following from the root of the repository.
```
$ docker-compose up
```

API documentation can be viewed at [http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html).

![Swagger](images/swagger.png)

## Schema
```
# User
id
username
password

# RefreshToken
id
token
userId

# Topic
id
name
description
userId
created

# Post
id
title
text
userId
topicId
created

# Comment
id
parentId
text
postId
userId
created
```

## Tools
The following tools were used for development.
- [IntelliJ IDEA](https://www.jetbrains.com/idea/)
- [SQuirreL SQL](http://squirrel-sql.sourceforge.net/), [PostgreSQL JDBC driver](https://jdbc.postgresql.org/download.html)

## TODO
- A frontend!!!
